import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Bags from '@/components/Bags'
import Find from '@/components/Find'
import Mine from '@/components/Mine'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/bags',
      name: 'Bags',
      component: Bags
    }, {
      path: '/find',
      name: 'Find',
      component: Find
    }, {
      path: '/mine',
      name: 'Mine',
      component: Mine
    }
  ]
})
